$(document).ready(function() {
    changePageAndSize();
});

function changePageAndSize() {
    $('#pageSizeSelect').change(function(evt) {
        window.location.replace("/viewPayments?pageSize=" + this.value + "&page=1&statusId=" +$('#statusId').val());
    });
}
