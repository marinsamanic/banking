package com.msamanic.banking.repository;

import com.msamanic.banking.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    BankAccount findByBankAccountId(Long bankAccountId);

    BankAccount findByAccountNumber(String accountNumber);

}
