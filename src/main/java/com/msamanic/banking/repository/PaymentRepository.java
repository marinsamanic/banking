package com.msamanic.banking.repository;

import com.msamanic.banking.model.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    Payment findByPaymentId(Long paymentId);

    Page<Payment> findByStatusIn(Pageable pageRequest, List<Short> statuses);

    Page<Payment> findByStatusInAndPayerBankAccountUserId(Pageable pageRequest, List<Short> statuses, Long id);

}
