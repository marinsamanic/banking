package com.msamanic.banking.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface CounterRepositoryCustom {

    Long getCountByName(String name);
}
