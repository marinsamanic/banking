package com.msamanic.banking.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class CounterRepositoryImpl implements CounterRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false, isolation = Isolation.DEFAULT,
            propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public Long getCountByName(String name) {
        Query query = entityManager.createQuery("update Counter c set c.count = c.count + 1 where c.name = :name");
        query.setParameter("name", name);
        query.executeUpdate();

        query = entityManager.createQuery("select c.count from Counter c where c.name = :name");
        query.setParameter("name", name);

        return (Long) query.getSingleResult();
    }
}
