package com.msamanic.banking.repository;

import com.msamanic.banking.model.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CounterRepository extends JpaRepository<Counter, Long>, CounterRepositoryCustom {

    Counter findByName(String name);

    @Modifying
    @Query("update Counter c set c.count = c.count + 1 where c.name = ?1")
    @Transactional(readOnly = false, isolation = Isolation.DEFAULT,
            propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    void updateCounterByName(String counterName);

}
