package com.msamanic.banking.web;

import com.msamanic.banking.model.User;
import com.msamanic.banking.service.UserService;
import com.msamanic.banking.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                      BindingResult result){

        User existing = userService.findByEmail(userDto.getEmail());
        if (existing != null){
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        existing = userService.findByEmail(userDto.getEmail());
        if (existing != null){
            result.rejectValue("username", null, "Username already exists");
        }

        if (userDto.getTerms() != true){
            result.rejectValue("terms", null, "You must agree with terms");
        }

        if (result.hasErrors()){
            return "registration";
        }

        userService.createAndSaveUser(userDto);
        return "redirect:/registration?success";
    }

}
