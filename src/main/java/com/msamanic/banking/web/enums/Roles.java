package com.msamanic.banking.web.enums;

public enum Roles {
    ROLE_ADMIN, ROLE_USER;
}
