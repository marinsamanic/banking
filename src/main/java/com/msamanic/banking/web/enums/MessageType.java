package com.msamanic.banking.web.enums;

public enum MessageType {
    INFO, ERR;
}
