package com.msamanic.banking.web;

import com.msamanic.banking.config.LoggedUser;
import com.msamanic.banking.exceptions.InsufficientBalanceException;
import com.msamanic.banking.exceptions.InvalidAcountException;
import com.msamanic.banking.model.Payment;
import com.msamanic.banking.model.enums.PaymentStatus;
import com.msamanic.banking.service.PaymentService;
import com.msamanic.banking.service.UserService;
import com.msamanic.banking.web.dto.Message;
import com.msamanic.banking.web.dto.PaymentDto;
import com.msamanic.banking.web.enums.MessageType;
import com.msamanic.banking.web.enums.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Controller
public class PaymentController {
    private final static Logger LOGGER = Logger.getLogger(PaymentController.class.getName());

    private static final int BUTTONS_TO_SHOW = 3;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;
    private static final int[] PAGE_SIZES = { 5, 10};

    @Autowired
    private UserService userService;

    @Autowired
    private PaymentService paymentService;

    @ModelAttribute("payment")
    public PaymentDto paymentDto() {
        return new PaymentDto();
    }

    @ModelAttribute("messages")
    public List<Message> message() {
        return new ArrayList<>();
    }

    @RequestMapping(path="/user/makePayment", method= RequestMethod.GET)
    public String showPaymentForm(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        LoggedUser user = (LoggedUser) auth.getPrincipal();
        model.addAttribute("senderAccount", user.getBankAccount().getAccountNumber());

        return "makePayment";
    }

    @RequestMapping(path="/user/makePayment", method= RequestMethod.POST)
    public String addPayment(Model model,
                             @ModelAttribute("payment") @Valid PaymentDto paymentDto,
                             BindingResult result){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        LoggedUser user = (LoggedUser) auth.getPrincipal();
        model.addAttribute("senderAccount", user.getBankAccount().getAccountNumber());

        if (!result.hasErrors()) {
            try {
                paymentService.addNewPayment(user.getBankAccount().getBankAccountId(), paymentDto);
            } catch (InsufficientBalanceException e) {
                result.reject("amount", "Payment unsuccessful. Insufficient balance!");
            } catch (InvalidAcountException e) {
                result.reject("accountNumber", "Payment unsuccessful. " + e.getMessage());
            }
        }
        if (result.hasErrors()) {
            return "makePayment";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping ("/viewPayments")
    public ModelAndView viewPayments(@RequestParam("statusId") Optional<Short> statusId,
                                     @RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> page,
                                     @ModelAttribute("messages") List<Message> messages,
                                     BindingResult mapping1BindingResult,
                                     Model model){

        ModelAndView modelAndView = new ModelAndView("viewPayments");
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        List<Short> statusIds = new ArrayList<>();
        if (statusId.isPresent()) {
            statusIds.add(statusId.get());
        } else {
            for (PaymentStatus status : PaymentStatus.values()) {
                statusIds.add(status.getStatusId());
            }
        }

        Long userId = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        LoggedUser user = (LoggedUser) auth.getPrincipal();
        if (user.hasRole(Roles.ROLE_USER.name())) {
            userId = user.getId();
        }

        Page<Payment> paymentList = paymentService.getPayments(new PageRequest(evalPage, evalPageSize,
                Sort.Direction.ASC, "created"), userId, statusIds);

//        Page<Payment> paymentList2 = paymentRepository.findByStatusInAndPayerBankAccountUserId(new PageRequest(evalPage, evalPageSize,
//                Sort.Direction.ASC, "created"), statusIds, (long) 16);

        PagerModel pager = new PagerModel(paymentList.getTotalPages(),paymentList.getNumber(),BUTTONS_TO_SHOW);
        modelAndView.addObject("paymentList",paymentList);
        modelAndView.addObject("statuses", PaymentStatus.values());
        modelAndView.addObject("statusId", statusId.orElse(null));
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pageSizes", PAGE_SIZES);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @RequestMapping (value = "/approvePayment", method = RequestMethod.POST)
    public ModelAndView approvePayment(@ModelAttribute("messages") List<Message> messages,
                                       @RequestParam("paymentId") Long paymentId,
                                       @RequestParam("statusId") Optional<Short> statusId,
                                       @RequestParam("pageSize") Optional<Integer> pageSize,
                                       BindingResult mapping1BindingResult,
                                       Model model,
                                       RedirectAttributes redirectAttributes){

        redirectAttributes.addFlashAttribute("messages", messages);
        try {
            paymentService.approvePayment(paymentId);
            messages.add(new Message(MessageType.INFO, "Payment approved successfully!"));
        } catch (InsufficientBalanceException e) {
            messages.add(new Message(MessageType.ERR, "Payment not approved. Insifficient funds!"));
        } catch (RuntimeException e) {
            messages.add(new Message(MessageType.ERR, "Unexpected error!"));
        }

        ModelAndView modelAndView = new ModelAndView("redirect:/viewPayments");

        if (statusId.isPresent()) {
            modelAndView.addObject("statusId", statusId.get());
        }
        if (pageSize.isPresent()) {
            modelAndView.addObject("pageSize", pageSize.get());
        }

        return modelAndView;
    }

    @RequestMapping (value = "/declinePayment", method = RequestMethod.POST)
    public ModelAndView declinePayment (@RequestParam("paymentId") Long paymentId,
                                        @RequestParam("statusId") Optional<Short> statusId,
                                        @RequestParam("pageSize") Optional<Integer> pageSize,
                                        @ModelAttribute("messages") List<Message> messages,
                                        BindingResult mapping1BindingResult,
                                        Model model,
                                        RedirectAttributes redirectAttributes){

        redirectAttributes.addFlashAttribute("messages", messages);
        try {
            paymentService.declinePayment(paymentId);
            messages.add(new Message(MessageType.INFO, "Payment declined successfully!"));
        } catch (RuntimeException e) {
            messages.add(new Message(MessageType.ERR, "Unexpected error!"));
        }

        ModelAndView modelAndView = new ModelAndView("redirect:/viewPayments");

        if (statusId.isPresent()) {
            modelAndView.addObject("statusId", statusId.get());
        }
        if (pageSize.isPresent()) {
            modelAndView.addObject("pageSize", pageSize.get());
        }

        return modelAndView;
    }
}
