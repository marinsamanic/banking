package com.msamanic.banking.web.dto;

import com.msamanic.banking.web.enums.MessageType;

public class Message {
    private MessageType messageType;
    private String message;

    public Message() {
    }

    public Message(MessageType messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
