package com.msamanic.banking.exceptions;

public class InvalidAcountException extends Exception {

    public InvalidAcountException(String message) {
        super(message);
    }
}
