package com.msamanic.banking.service;

import com.msamanic.banking.exceptions.InsufficientBalanceException;
import com.msamanic.banking.exceptions.InvalidAcountException;
import com.msamanic.banking.model.Payment;
import com.msamanic.banking.web.dto.PaymentDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PaymentService {

    void addNewPayment(Long payeeBankAccountId, PaymentDto paymentDto) throws InsufficientBalanceException, InvalidAcountException;

    List<Payment> getAllPayments();

    Payment getPaymentById(Long id);

    void approvePayment(Long id) throws InsufficientBalanceException;

    void declinePayment(Long id);

    Page<Payment> getPayments(Pageable pageRequest, Long userId, List<Short> statuses);
}
