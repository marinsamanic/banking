package com.msamanic.banking.service;

import com.msamanic.banking.exceptions.InsufficientBalanceException;
import com.msamanic.banking.exceptions.InvalidAcountException;
import com.msamanic.banking.model.BankAccount;
import com.msamanic.banking.model.Payment;
import com.msamanic.banking.model.enums.CounterName;
import com.msamanic.banking.model.enums.PaymentStatus;
import com.msamanic.banking.repository.BankAccountRepository;
import com.msamanic.banking.repository.CounterRepository;
import com.msamanic.banking.repository.PaymentRepository;
import com.msamanic.banking.web.dto.PaymentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Short MANUAL_PROCESS_EVERY_N = 10;
    private static final BigDecimal MAX_AMOUNT_FOR_AUTOMATIC_APPROVAL = new BigDecimal(100);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private CounterRepository counterRepository;

    @Override
    public void addNewPayment(Long payerBankAccountId, PaymentDto paymentDto) throws InsufficientBalanceException, InvalidAcountException {

        BankAccount payerBankAccount = bankAccountRepository.findByBankAccountId(payerBankAccountId);
        BankAccount payeeBankAccount = bankAccountRepository.findByAccountNumber(paymentDto.getAccountNumber());

        if (payeeBankAccount == null) {
            throw new InvalidAcountException("Entered account number doesn't exist");
        } else if (payeeBankAccount.getBankAccountId().equals(payerBankAccountId)) {
            throw new InvalidAcountException("Account number cannot be the same as sender's account number!");
        }

        Payment payment = new Payment();
        payment.setPayeeBankAccount(payeeBankAccount);
        payment.setPayerBankAccount(payerBankAccount);
        payment.setAmount(paymentDto.getAmount());
        payment.setCreated(new Date());

        if (!isSufficientBanlance(payerBankAccount, payment.getAmount())) {
            payment.setStatus(PaymentStatus.DECLINED.getStatusId());
            payment.setProcessed(new Date());
            paymentRepository.save(payment);
            throw new InsufficientBalanceException("Insufficient balance");
        } else {
            if (!shouldApproveManualy(payment.getAmount())) {
                payment.setStatus(PaymentStatus.APPROVED.getStatusId());
                payment.setProcessed(new Date());

                payerBankAccount.setBalance(payerBankAccount.getBalance().subtract(payment.getAmount()));
                payeeBankAccount.setBalance(payeeBankAccount.getBalance().add(payment.getAmount()));
            } else {
                payment.setStatus(PaymentStatus.CREATED.getStatusId());
            }
        }

        paymentRepository.save(payment);
        bankAccountRepository.save(payerBankAccount);
        bankAccountRepository.save(payeeBankAccount);
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentRepository.getOne(id);
    }

    @Override
    public void approvePayment(Long id) throws InsufficientBalanceException {
        Payment payment = paymentRepository.findOne(id);
        if (isSufficientBanlance(payment.getPayerBankAccount(), payment.getAmount())) {
            payment.setStatus(PaymentStatus.APPROVED.getStatusId());
            payment.setProcessed(new Date());

            BankAccount payerBankAccount = payment.getPayerBankAccount();
            BankAccount payeeBankAccount = payment.getPayeeBankAccount();

            payerBankAccount.setBalance(payerBankAccount.getBalance().subtract(payment.getAmount()));
            payeeBankAccount.setBalance(payeeBankAccount.getBalance().add(payment.getAmount()));

            paymentRepository.save(payment);
            bankAccountRepository.save(payerBankAccount);
            bankAccountRepository.save(payeeBankAccount);

        } else {
            payment.setStatus(PaymentStatus.DECLINED.getStatusId());
            payment.setProcessed(new Date());
            paymentRepository.save(payment);
            throw new InsufficientBalanceException("Insufficient balance");
        }
    }

    @Override
    public void declinePayment(Long id) {
        Payment payment = paymentRepository.findOne(id);
        payment.setStatus(PaymentStatus.DECLINED.getStatusId());
        payment.setProcessed(new Date());

        paymentRepository.save(payment);
    }

    @Override
    public Page<Payment> getPayments(Pageable pageRequest, Long userId, List<Short> statuses) {
        if (userId == null) {
            return paymentRepository.findByStatusIn(pageRequest, statuses);
        } else {
            return paymentRepository.findByStatusInAndPayerBankAccountUserId(pageRequest, statuses, userId);
        }
    }

    private boolean isSufficientBanlance(BankAccount payerBankAccount, BigDecimal amount) {
        if (payerBankAccount.getBalance().compareTo(amount) == -1) {
            return false;
        } else {
            return true;
        }
    }

    private boolean shouldApproveManualy(BigDecimal amount) {
        Long count = counterRepository.getCountByName(CounterName.PAYMENT_COUNTER.name());
        if (amount.compareTo(MAX_AMOUNT_FOR_AUTOMATIC_APPROVAL) >= 0) {
            return true;
        } else {
            if (count % MANUAL_PROCESS_EVERY_N == 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}
