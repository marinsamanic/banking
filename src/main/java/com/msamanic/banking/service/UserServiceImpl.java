package com.msamanic.banking.service;

import com.msamanic.banking.config.LoggedUser;
import com.msamanic.banking.model.BankAccount;
import com.msamanic.banking.model.Role;
import com.msamanic.banking.model.User;
import com.msamanic.banking.model.enums.CounterName;
import com.msamanic.banking.repository.CounterRepository;
import com.msamanic.banking.repository.RoleRepository;
import com.msamanic.banking.repository.UserRepository;
import com.msamanic.banking.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final BigDecimal INITIAL_AMOUNT = new BigDecimal(1000);
    private static final String DEFAULT_CURRENCY_CODE = "HRK";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CounterRepository counterRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User createAndSaveUser(UserRegistrationDto registration){
        User user = new User();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setUsername(registration.getUsername());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        Role role = roleRepository.findByName("ROLE_USER");
        user.setRoles(Arrays.asList(role));

        BankAccount bankAccount = new BankAccount();

        Long count = counterRepository.getCountByName(CounterName.ACCOUNT_NUMBER_COUNTER.name());

        String accountNumber = fillZeros(count.toString(), 10);
        bankAccount.setAccountNumber(accountNumber);

        bankAccount.setBalance(INITIAL_AMOUNT);
        bankAccount.setCurrencyCode(DEFAULT_CURRENCY_CODE);

        bankAccount.setUser(user);
        user.setBankAccounts(Arrays.asList(bankAccount));

        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        //model dopušta više transakcijskih računa, a zadatak je ograničen na 1, pa uzimamo prvi iz liste
        BankAccount bankAccount = null;
        if (!user.getBankAccounts().isEmpty()) {
            bankAccount = user.getBankAccounts().iterator().next();
        }

        return new LoggedUser(user.getUsername(),
                user.getPassword(), mapRolesToAuthorities(user.getRoles()), user.getId(), bankAccount);
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    public void dummyInitBanker() {
        User user = userRepository.findByUsername("banker");
        if (user == null) {
            user = new User();
            user.setUsername("banker");
            user.setFirstName("Banker");
            user.setLastName("Banker");
            user.setEmail("banker@banker");
            user.setPassword(passwordEncoder.encode("banker"));
            Role role = roleRepository.findByName("ROLE_ADMIN");
            user.setRoles(Arrays.asList(role));
            userRepository.save(user);
        }
    }

    private String fillZeros(String stringToFill, Integer finalLength) {
        while (stringToFill.length() < finalLength) {
            stringToFill = "0" + stringToFill;
        }
        return stringToFill;
    }
}
