package com.msamanic.banking.service;

import com.msamanic.banking.model.User;
import com.msamanic.banking.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User findByUsername(String username);

    User createAndSaveUser(UserRegistrationDto registration);

    void dummyInitBanker();
}
