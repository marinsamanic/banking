package com.msamanic.banking.config;

import com.msamanic.banking.model.BankAccount;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class LoggedUser extends User {

    private Long id;
    private BankAccount bankAccount;

    public LoggedUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public LoggedUser(String username, String password, Collection<? extends GrantedAuthority> authorities,
                      Long id, BankAccount bankAccount) {
        super(username, password, authorities);
        this.bankAccount = bankAccount;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public boolean hasRole(String role) {
        for (GrantedAuthority authority : getAuthorities()) {
            if (authority.getAuthority().equals(role)) {
                return true;
            }
        }
        return false;
    }
}
