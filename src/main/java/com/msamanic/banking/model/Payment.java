package com.msamanic.banking.model;

import com.msamanic.banking.model.enums.PaymentStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table
public class Payment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long paymentId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payer_account_id", nullable = false)
    private BankAccount payerBankAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payee_account_id", nullable = false)
    private BankAccount payeeBankAccount;

    private BigDecimal amount;
    private Date created;
    private Date processed;
    private Short status;

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public BankAccount getPayerBankAccount() {
        return payerBankAccount;
    }

    public void setPayerBankAccount(BankAccount payerBankAccount) {
        this.payerBankAccount = payerBankAccount;
    }

    public BankAccount getPayeeBankAccount() {
        return payeeBankAccount;
    }

    public void setPayeeBankAccount(BankAccount payeeBankAccount) {
        this.payeeBankAccount = payeeBankAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getProcessed() {
        return processed;
    }

    public void setProcessed(Date processed) {
        this.processed = processed;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Transient
    public PaymentStatus getPaymentStatus() {
        return PaymentStatus.getEnumById(status);
    }
}
