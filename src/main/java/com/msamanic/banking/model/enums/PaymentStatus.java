package com.msamanic.banking.model.enums;

public enum PaymentStatus {
    CREATED((short)0, "Created"),
    APPROVED((short)1, "Approved"),
    DECLINED((short)2, "Declined");

    private Short statusId;
    private String statusText;

    PaymentStatus(Short statusId, String statusText) {
        this.statusId = statusId;
        this.statusText = statusText;
    }

    public Short getStatusId() {
        return statusId;
    }

    public void setStatusId(Short statusId) {
        this.statusId = statusId;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public static PaymentStatus getEnumById(Short id) {
        for (PaymentStatus status : PaymentStatus.values()) {
            if (status.getStatusId().equals(id)) {
                return status;
            }
        }
        return null;
    }
}
