package com.msamanic.banking.model.enums;

public enum CounterName {
    ACCOUNT_NUMBER_COUNTER,
    PAYMENT_COUNTER;
}
