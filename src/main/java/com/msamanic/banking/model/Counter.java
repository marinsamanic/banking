package com.msamanic.banking.model;

import javax.persistence.*;

@Entity
@Table
public class Counter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long counterId;

    private Long count;
    private String name;

    public Long getCounterId() {
        return counterId;
    }

    public void setCounterId(Long counterId) {
        this.counterId = counterId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
